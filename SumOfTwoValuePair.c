/**************************************************************************************************************************************

 	NAME					EMAIL-ID		PHONE NUMBER         EMPLOYEE-ID
----------------------------------------------------------------------------------------------------------------------------------------	    M.VIDYA VATHI			mvidyareddy1212@gmail.com	  9603477198            10358

----------------------------------------------------------------------------------------------------------------------------------------

This program is an implementaion of finding the sum of the two values equals to the given data in the array. 
***************************************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#define SIZE 5
//function to find the sum of values equal to the given data
int* getArray(int* arr,int* arr1,int data){
	int i,j;
	arr1=(int*)malloc(2*(sizeof(int)));
	for(i=0,j=SIZE-1;i<j;){
		//checking the conditions if the sum of the two values in the array 
		//is equal to the given data or not.
		if((arr[i]+arr[j])==data){
				arr1[0]=i;
				arr1[1]=j;
				return arr1;
			}else if((arr[i]+arr[j])>data){
				j--;
			}else{
				i++;
		}
	}
	return NULL;
}
int main(){
	int arr[SIZE]={5,7,25,66,87};
	int *arr1,data;
	printf("Enter data \n");
	scanf("%d",&data);
	arr1=getArray(arr,arr1,data);
	//checking the conditions if the returned array is equal to null or not 
	// if yes then print sum is  not found statement otherwise print indexes of the pair values 	
	if(arr1==NULL){
		printf("Sum of the values for %d is not found\n",data);
	}else{
		printf("values present in the indexes i=%d j=%d is equal to the given data %d\n",arr1[0],arr1[1],data);
	}
return 0;
}
/************************************************OUTPUT OF THE ABOVE PROGRAM*****************************************************
Enter data 
71
values present in the indexes i=0 j=3 is equal to the given data 71

-**************************************************************************************************************************************/ 
