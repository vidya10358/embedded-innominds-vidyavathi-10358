/**************************************************************************************************
  NAME                  PHONE NUMBER           EMAIL ID

 M.VIDYAVATHI		9603477198             mvidyareddy1212@gmail.com

***************************************************************************************************
   This program is for the generating the matrix by taking random number of rows 
   and columns generated by random function and then we are 
   finding the number of equilateral triangle formed. 
***************************************************************************************************/

#include<cstdio>
#include<cstdlib>
#include<unistd.h>
#include<iostream>
#include<vector>
using namespace std;
//decalring the class mytriangle
class Mytriangle {

	private:
		//decalaring variables
		int row,column,i,j,cnt;
		//creating vector object
		vector<vector<int> > matrix;
	public:
		//creating default constructor for Mytriangle
		Mytriangle() {
			srand(getpid());
			row=random()%8;
			column=random()%8;
			for(int i = 0; i < row; ++i) {
				matrix.push_back(vector<int>());
				for(j=0;j<column;j++)
					matrix[i].push_back(random()%10);
			}
		}
		//method to print the elements of an array
		void printMatrix() {
			for(i=0;i<row;i++,cout<<endl) {
				for(j=0;j<column;j++) {
					cout<<matrix[i][j]<<" ";
				}
			}
		}
		//method to find the number of triangles generated.
		void findTriangles() {
			int n;
			cnt=0;
			int num=0;
			for(n=1;n<=row-1;n++) {
				num=0;

				for(i=0;i<row-n;i++) {
					for(j=n;j<column-n;j++) {

						if(matrix[i][j]>0&&matrix[i+n][j-n]>0 && matrix[i+n][j+n]>0) {
							num++;
							cnt++;

						}

					}

				}
				cout<<"number of triangles in"<<n<<"hubs :"<<num<<endl;
			}

			cout<<"total number of triangles:"<<cnt<<endl;
		}

};

int main(void) {
	//creating object of type Mytriangle
	Mytriangle mytriangle;
	mytriangle.printMatrix();
	mytriangle.findTriangles();
	return 0;
}

/*************************************output of the program****************************************
imvizag@administrator-ThinkCentre-M82:~/grossary$ ./a.out 
3 3 9 9 9 
6 3 7 7 8 
4 2 3 1 7 
2 4 4 2 9 
6 4 3 8 0 
number of triangles in1hubs :11
number of triangles in2hubs :2
number of triangles in3hubs :0
number of triangles in4hubs :0
total number of triangles:13
****************************************************************************************************/
