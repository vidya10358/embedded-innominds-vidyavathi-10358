/********************************************************************************************************************
  NAME                  PHONE NUMBER           EMAIL ID			      employeeid
___________________________________________________________________________________________________________________
 
M.VIDYAVATHI           9603477198             mvidyareddy1212@gmail.com	10358
____________________________________________________________________________________________________________________

This program is an implemetation  of the matrix rotation

*********************************************************************************************************************/

 
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
int main(){
	//declaring the variables
	int i,j,k,row,column;
	srand(getpid());
	//entering the rows and coloumns
	printf("Enter row value  ");
	scanf("%d",&row);
	printf("Enter colums value  ");
	scanf("%d",&column);
	//declaring the resultant matrix
	int arr1[column][row];
	//declaring the input matrix
	int arr[row][column];
		for(i=0;i<row;i++){
			for(j=0;j<column;j++){
				//generating the elements randomly by using rand function
				arr[i][j]= random()%10;
				
		}
	}
	//printing the matrix befor rotation
	printf("matrix before rotation:");
	printf("\n");
	for(i=0;i<row;i++){
		for(j=0;j<column;j++){
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
	//logic for rotating the matrix
	for(i=0,j=row-1;i<row && j>=0;i++,j--){
			for(k=0;k<column;k++)
				arr1[k][j]=arr[i][k];
		
	}
	//printing the matrix after rotation
	printf("matrix after rotation:");
	printf("\n");
	for(i=0;i<column;i++){
		for(j=0;j<row;j++){
			printf("%d ",arr1[i][j]);
		}
		printf("\n");
	}
	return 0;

}

/***********************************************OUTPUT OF THE ABOVE PROGRAM****************************************
Enter row value  6
Enter colums value  7
matrix before rotation:
1 0 5 4 0 5 7 
3 4 0 8 0 8 4 
4 7 2 1 8 5 5 
6 7 8 0 3 9 7 
2 9 9 3 1 4 9 
1 1 8 4 7 8 4 
matrix after rotation:
1 2 6 4 3 1 
1 9 7 7 4 0 
8 9 8 2 0 5 
4 3 0 1 8 4 
7 1 3 8 0 0 
8 4 9 5 8 5 
4 9 7 5 4 7 
************************************************************************************************************************/ 
