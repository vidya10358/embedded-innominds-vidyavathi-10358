/******************************************************************************************************
NAME                           MOBILENUMBER                     EMAILID                   EMPLOYEEID
-------------------------------------------------------------------------------------------------------
M.VIDYAVATHI                   9603477198                   mvidyareddy1212@gmail.com       10358
-------------------------------------------------------------------------------------------------------
This program is the implementation of stack using linkedlist where user can push the data into the stack 
and pop the data from the stack 
*******************************************************************************************************/
#include<iostream>
#include<stdlib.h>

using namespace std;
//creating the structure of type node
struct node
{
	int data;
	struct node *next;
};
//creating the stack class
class Stack
{
	//declaring the pointer top
	struct node *top;
	public:
	//default constructor for stack
	Stack()
	{
		top=NULL;
	}
	//methods declaration
	int push(int);
	int pop();
	void displayDataInTheStack();
};
//mathod for push
int Stack :: push(int data)
{
	struct node *newnode,*temp;
	newnode = new node;
	newnode->data = data;
	newnode->next = NULL;
	if(top==NULL)
	{
		top = newnode;
		return 0;
	}
	for(temp = top;temp->next != NULL;temp = temp->next);
	temp->next = newnode;
	return 0;
}
//method for pop
int Stack :: pop()
{
	struct node *temp,*prev;
	int data;
	if(top==NULL)
	{
		cout<<"stack is empty"<<endl;
		return 0;
	}
	if(top->next==NULL)
	{
		data = top->data;
		top = NULL;
		return data;
	}
	for(temp = top;temp->next != NULL;prev = temp,temp = temp->next);
	prev->next = NULL;
	data = temp->data;
	delete temp;
	temp = NULL;
	return data;
}
//method for display the data in the stack
void Stack :: displayDataInTheStack()
{
	if(top==NULL)
	{
		cout<<"stack underflow"<<endl;
	}
	else
	{
		struct node *temp;
		for(temp = top;temp != NULL;temp = temp->next)
		cout<<temp->data<<endl;
	}
}
//main method
 main()
{	
	Stack stack;
	int choice;
	int data;
	while(1)
	{
		cout<<"************MENU*******************"<<endl;
		cout<<"1.Push data into stack"<<endl<<"2.pop the data from the stack"<<endl<<"3.display the data in the stack"<<endl<<"4.exit"<<endl;
		cout<<"enter ur choice";
		cin>>choice;
		switch(choice)
		{
			case 1: cout<<"enter the data to be push:";
				cin>>data;
				data=stack.push(data);
				break;
			case 2: data =stack.pop();
				if(data)
				cout<<"poped value"<<" "<<data<<endl;
				break;
			case 3:stack.displayDataInTheStack();
				break;
			case 4:exit(0);
			default:cout<<"invalid case"<<endl;
		}
	}

}
/***************************************OUTPUT OF THE ABOVE PROGRAM***************************************
imvizag@administrator-OptiPlex-3020:~/linkedlist$ ./a.out
************MENU*******************
1.Push data into stack
2.pop the data from the stack
3.display the data in the stack
4.exit
enter ur choice1
enter the data to be push:2
************MENU*******************
1.Push data into stack
2.pop the data from the stack
3.display the data in the stack
4.exit
enter ur choice1
enter the data to be push:3
************MENU*******************
1.Push data into stack
2.pop the data from the stack
3.display the data in the stack
4.exit
enter ur choice1
enter the data to be push:5
************MENU*******************
1.Push data into stack
2.pop the data from the stack
3.display the data in the stack
4.exit
enter ur choice3
2
3
5
************MENU*******************
1.Push data into stack
2.pop the data from the stack
3.display the data in the stack
4.exit
enter ur choice2
poped value 5
************MENU*******************
1.Push data into stack
2.pop the data from the stack
3.display the data in the stack
4.exit
enter ur choice2
poped value 3
************MENU*******************
1.Push data into stack
2.pop the data from the stack
3.display the data in the stack
4.exit
enter ur choice3
2
************MENU*******************
1.Push data into stack
2.pop the data from the stack
3.display the data in the stack
4.exit
enter ur choice4*/
	
