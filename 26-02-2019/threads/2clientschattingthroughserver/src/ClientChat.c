#include"Client.h"
//declaring the variables
int n;
int sockfd,portNum;
//declaring the character buffer
char buf[1024];
char buff[4]={'b','y','e','\n'};
//function for printing the error message
void error(char *msg){
	perror(msg);
	exit(1);
}
//function for client reading
void * clientRead(void* arg) {
	while(1)
	{
		memset(buf,0,sizeof(buf));
		n = read(sockfd,buf,1024);
		if(n<0)
			error("ERROR ON READING");
		if(!(strcmp(buf,buff))) {
			
			printf("client=%s\n",buf);
			exit(1);
		}
		else
			printf("client=%s\n",buf);
	}
	return NULL;
}
//function for client writing
void * clientWrite(void* arg) {
	while(1){
		memset(buf,0,sizeof(buf));
		fgets(buf,1024,stdin);
		n = write(sockfd,buf,strlen(buf));
		if(n<0)
		error("ERROR ON WRITING");
		if(!(strcmp(buf,buff))) 
			exit(1);
	}
	return NULL;
}

//In main method we are creating the socket and we are creating the threads,while creating the threads we are calling the client read and client write functions and we are joining the threads
int main(int argc, char **argv){
	struct hostent *server;
	pthread_t thread_id,thread_id1;
	struct sockaddr_in serv_addr;
	if(argc<3){
		fprintf(stderr,"usage %s hostname port\n",argv[0]);
		exit(0);
	}
	portNum = atoi(argv[2]);
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	server = gethostbyname(argv[1]);
	if(server == NULL){
		fprintf(stderr,"error,no such host\n");
		exit(0);
	}
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	strncpy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port = htons(portNum);
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
		error("ERROR IN CONNECTING");
	//thread creation
	pthread_create(&thread_id,NULL,clientRead , NULL);
	pthread_create(&thread_id1,NULL,clientWrite, NULL);
	//thread join
	pthread_join(thread_id, NULL);
	pthread_join(thread_id1, NULL);
	//closing the file descriptor
	close(sockfd);
	return 0;
}
