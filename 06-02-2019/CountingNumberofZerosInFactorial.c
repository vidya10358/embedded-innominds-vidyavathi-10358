/************************************************************************************************************
    NAME                MOBILE NUMBER                   EMAIL-ID                        EMPLOYEE-ID
------------------------------------------------------------------------------------------------------------
M. VIDYAVATHI	         9603477198		mvidyareddy1212@gmail.com		  10358
***********************************************************************************************************

This program is to count the number of zeros in the last after finding the factorial of a given number.

*************************************************************************************************************/

#include<stdio.h>
#include<math.h>
int main(){
	int data,numberOfZeros = 0,c = 1;
	printf("Enter data to find number of zeros\n");
	scanf("%d",&data);
	while(data>=pow(5,c)){
		numberOfZeros = numberOfZeros+(data/pow(5,c));
		c++;
	}
	printf("Number of zeros in the given data is %d\n",numberOfZeros);
}


/****************************************OUTPUT***********************************************************
Enter data to find number of zeros
86
Number of zeros in the given data is 20
---------------------------------------------------------------------------------------------------------
Enter data to find number of zeros
34
Number of zeros in the given data is 7
--------------------------------------------------------------------------------------------------------
Enter data to find number of zeros
24
Number of zeros in the given data is 4
 
*********************************************************************************************************/
