/******************************************************************************************************************************
 	NAME			PHONE NUMBER 			EMAIL-ID 			EMPLOYEE-ID
-------------------------------------------------------------------------------------------------------------------------------
 M. VIDYAVATHI                  9603477198		mvidyareddy1212@gmal.com		  10358
-------------------------------------------------------------------------------------------------------------------------------
In this program we find the sub array whose sum of maximum values in the given array.
******************************************************************************************************************************/

#include<stdio.h>
#define SIZE 10

//declaring the functions
int maxSubArraySum(int a[],int size);
int max(int a,int b);
int main(){
	//decalring the variables
	int i,result;
	//declaring and initializing the array
	int arr[SIZE] = {1,2,-3,4,5,7,-4,3,-2,-3};
	for(i=0;i<SIZE;i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");
	result=maxSubArraySum(arr,SIZE);
	printf("maximum value=%d\n",result);
}

//function to find the subarray which has maximum value
int maxSubArraySum(int a[],int size){
	int i;
	int max_so_far = a[0];
	int curr_max = a[0];
	for(i=1;i<size;i++){
		curr_max = max(a[i],curr_max+a[i]);
		max_so_far = max(max_so_far,curr_max);

	}
	return max_so_far;
}

//function to finding the maximum value
int max(int a,int b){
	if(a>b){
		return a;
	}
	else{
		return b;
	}
}

/*******************************************OUTPUT FOR THE ABOVE PROGRAM**************************************************************
 busam@busam-Lenovo-G505:~/class$ ./a.out
1  2  -3  4  5  7  -4  3  -2  -3  
maximum value=16
**************************************************************************************************************************************/
