#include"headers.h"

//defination for the display list function
void displayList(struct Student *Node){
	//initialising the structure variable
	struct Student *temp = Node;

	//loop for traversing the list
	while(temp != NULL){
		//printing the data in the list
		printf("rollNo=%d name=%s\n",temp->rollNo,temp->name);
		temp = temp->next;
	}
}
