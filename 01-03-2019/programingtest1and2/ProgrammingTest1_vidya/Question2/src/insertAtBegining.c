#include"headers.h"

//defination for the insertat begin function
void insertAtBegining(struct Student **Node){
	//decalring the structure variable newnode
	struct Student *newnode;

	//allocating the memory for newnode 
	newnode = (struct Student *)malloc(sizeof(struct Student));
	//checking the condition if newnode is null or not
	if(newnode == NULL){
		printf("memory allocation failed");
		exit(0);
	}

	//entering the data into the list
	printf("enter the rollNumber of the student:");
	scanf("%d",&newnode->rollNo);
	printf("enter the name of the student:");
	scanf("%s",newnode->name);

	//logic for adding the node at begining
	newnode->next = *Node;
	*Node = newnode;
}
