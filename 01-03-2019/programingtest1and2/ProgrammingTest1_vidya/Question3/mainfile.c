#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(){

	//declaring the file descriptors
	FILE *fp1,*fp2,*fp3;
	int ch,ch1;
	//declaring the character buffer
	//opening the file descriptor
	fp1 = fopen("a.txt","r");
	//checking if file descriptor is null or not
	if(fp1 == NULL){
		printf("file is not opened");
		exit(0);
	}
	fp2 = fopen("b.txt","r");

	//checking if file descriptor is null or not
	if(fp1 == NULL){
		printf("file is not opened");
		exit(0);
	}
	
	fp3 = fopen("c.txt","a");

	//checking if file descriptor is null or not
	if(fp3 == NULL){
		printf("file is not opened");
		exit(0);
	}
	//logic for merging the lines in the two files
	x:while((ch=fgetc(fp1))!=-1) {
		if(ch==10) {
			fputc('\n',fp3);
			while((ch1=fgetc(fp2))!=-1) {
				if(ch1==10) {
					fputc('\n',fp3);
					goto x;
				} else {
				fputc(ch1,fp3);
				}
			}
		}else {
			fputc(ch,fp3);
		}

	}
	//closing the all file descriptors
	fclose(fp1);
	fclose(fp2);
	fclose(fp3);

	return 0;

}
