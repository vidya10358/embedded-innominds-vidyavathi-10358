#include<stdio.h>
#include<stdlib.h>


//main function
int main(){

	//declaring the  two file discriptors
	FILE *fp1,*fp2;

	//declaring the character buffer
	char buf[20],ch;
	int i=0,count = 0;

	//opening the sample.txt file in readmode
	fp1 = fopen("sample.txt","r");
	//checking if file descriptor is null or not 
	if(fp1 == NULL){
		printf("file is not open");
		exit(0);
	}

	//logic for counting the words
	while((ch = fgetc(fp1))){
		if(ch == ' ' || ch == '\n'){
			count++;
		}
		if(ch == EOF)
		break;
	}
	fp2 = fopen("resultfile.txt","w");
	//moving the file pointer to first position
	rewind(fp1);
	//checking if filedescriptor is null or not 
	if(fp2 == NULL){
		printf("file is not open");
		exit(0);
	}

	//logic for taking  the words in sample.txt and printing into resultant.txt
	for(i=0;i<count-1;i++){
		fscanf(fp1,"%s",buf);
		fprintf(fp2,"%s\n",buf);
	}

	//closing the two file descriptors 
	fclose(fp1);
	fclose(fp2);
}

