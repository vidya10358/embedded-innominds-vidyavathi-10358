//All header file
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<unistd.h>
#include<pthread.h>
#include<netdb.h>
#include<netinet/in.h>
#include<sys/socket.h>
//declaring the error function
void error(char *);
//declaring the read from client function
void* readFromClient(void *);
//declaring the write to client  function
void* writeToClient(void *);
//declaring the write to server function
void* writeToServer(void *);
//declaring the read from server functions
void* readFromServer(void *);

