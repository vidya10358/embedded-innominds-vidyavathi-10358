#include"headers.h"

//declaring the file descriptor and variables
int cfd,n;
//thread creation
pthread_t threadid1,threadid2;
//declaring the char buffer
char buf[1024];

//error function to print the error message
void error(char *msg){
	perror("msg");
	exit(0);
}

//write the data to the server function
void *writeToServer(void *args){
	while(1){
	memset(buf,0,sizeof(buf));
	fgets(buf,1024,stdin);
	n = write(cfd,buf,strlen(buf));
	if(n<0){
		printf("error on writing:");
	}
	//cheking the condition if the written data is equal to buf or not
	if(!(strncmp("bye",buf,3))){
		exit(1);
			}
	}
	 return NULL;

}

//read the data from the server function
void *readFromServer(void *args){
	while(1){
	memset(buf,0,sizeof(buf));
	n =read(cfd,buf,1024);
	if(n<0){
		printf("error on reading");
	}
	printf("server = %s\n",buf);
	//cheking the condition if the read data is equal to buf or not
	if(!(strncmp("bye",buf,3))){
		exit(1);
	}
	}
	return NULL;
}

//Driver function to drive the above functions
int main(int argc,char **argv){
	//declaring the structure variaables
	struct sockaddr_in serv_addr;
	struct hostent *server;
	if(argc<3){
		printf("you have entered less number of arguments:");
		exit(0);
	}
	//creating the socket
	cfd = socket(AF_INET,SOCK_STREAM,0);
	if(cfd<0){
		error("ERROR IN CREATING THE SOCKET");
		exit(0);
	}
	server = gethostbyname(argv[1]);
	memset((char *)&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(atoi(argv[2]));
	strncpy((char*)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,sizeof(server->h_length));
	//connecting to the server
	if(connect(cfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0){
		error("ERROR IN CONNECTING");
	}

	//thread creation
	pthread_create(&threadid1,NULL,writeToServer,NULL);
	pthread_create(&threadid2,NULL,readFromServer,NULL);

	//thread join
	pthread_join(threadid1,NULL);
	pthread_join(threadid2,NULL);

	//closing the file descriptors
	close(cfd);

}
