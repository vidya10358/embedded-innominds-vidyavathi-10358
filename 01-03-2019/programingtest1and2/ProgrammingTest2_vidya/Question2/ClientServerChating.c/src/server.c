#include"headers.h"
//declaring the file descriptors and variables
int sfd,cfd,n;
//declaring the threads
pthread_t threadid1,threadid2;
//declaring the character buffer
char buf[1024];
//error function for displaying the error
void error(char *msg){
	perror("msg");
	exit(0);
}
//readfromclient function for reading the data from client
void *readFromClient(void *args){
	while(1){
	memset(buf,0,sizeof(buf));
       	n = read(cfd,buf,1024);
	if(n<0){
		printf("error in reading");
	}
	printf("client = %s\n",buf);
	//checking the condition if the read buffer is bye or not
	if(!(strncmp("bye",buf,3))){
		exit(1);
	}
	}	
	return NULL;
}
//write the data to client function for writing the data to the client
void *writeToClient(void *args){
	while(1){
	memset(buf,0,sizeof(buf));
	fgets(buf,1024,stdin);
	n = write(cfd,buf,strlen(buf));
	if(n<0){
		printf("error on writing");
	}
	//checking the condition if the write buffer is bye or not
	if(!(strncmp("bye",buf,3))){
		exit(1);
	}
	}
	return NULL;
}
//Driver program to run all the above functions
int main(int argc,char **argv){
	//declaring  the structure variables
	struct sockaddr_in serv_addr,clien_addr;
	socklen_t clien;
	//checking the argument count
	if(argc<2){
		printf("you have entered less number of arguments:");
		exit(0);
	}
	//creating the socket
	sfd = socket(AF_INET,SOCK_STREAM,0);
	if(sfd<0){
		error("ERROR IN CREATING THE SOCKET");
		exit(0);
	}

	memset((char *)&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(atoi(argv[1]));
	serv_addr.sin_addr.s_addr = INADDR_ANY;

	//bind the socket
	if(bind(sfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0){
		error("ERROR IN BINDING");
		exit(0);
	}

	//listen function
	listen(sfd,1);
	clien = sizeof(clien_addr);

	//accepting the client
	cfd = accept(sfd,(struct sockaddr *)&clien_addr,&clien);
	if(cfd<0){
		error("ERROR ON ACCEPTING");
	}

	//thread creation
	pthread_create(&threadid1,NULL,readFromClient,NULL);
	pthread_create(&threadid2,NULL,writeToClient,NULL);

	//threadjoin
	pthread_join(threadid1,NULL);
	pthread_join(threadid2,NULL);

	//closing the file discriptors
	close(sfd);
	close(cfd);


		
}
