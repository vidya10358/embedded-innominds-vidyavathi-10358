#include"headers.h"

//error function for displaying the error
void error(char *msg){
	perror("msg");
	exit(0);
}
//Driver program to run all the above functions
int main(int argc,char **argv){
	//declaring the file descriptors and variables
	int sfd,cfd;
	//declaring  the structure variables
	struct sockaddr_in serv_addr,clien_addr;
	socklen_t clien;
	//checking the argument count
	if(argc<2){
		printf("you have entered less number of arguments:");
		exit(0);
	}
	//creating the socket
	sfd = socket(AF_INET,SOCK_STREAM,0);
	if(sfd<0){
		error("ERROR IN CREATING THE SOCKET");
		exit(0);
	}

	memset((char *)&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(atoi(argv[1]));
	serv_addr.sin_addr.s_addr = INADDR_ANY;

	//bind the socket
	if(bind(sfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0){
		error("ERROR IN BINDING");
		exit(0);
	}
	
	//listen function
	listen(sfd,1);
	clien = sizeof(clien_addr);

	//accepting the client
	cfd = accept(sfd,(struct sockaddr *)&clien_addr,&clien);
	if(cfd<0){
		error("ERROR ON ACCEPTING");
	}

	//closing the file discriptors
	close(sfd);
	close(cfd);


		
}
