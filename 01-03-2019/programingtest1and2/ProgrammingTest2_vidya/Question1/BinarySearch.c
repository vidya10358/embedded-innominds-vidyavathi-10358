#include<stdio.h>

//function for searching the element in the array
int binarySearch(int low,int high,int array[],int data){
	int mid;
	if(low>high){
		return -1;
	}
	mid = (low+high)/2;
	if(array[mid] == data){
		return mid;
	}
	else if(array[mid]>data){
		high = mid-1;
		return binarySearch(low,high,array,data);
	}
	else {
		low =mid+1;
		return binarySearch(low,high,array,data);
	
	}
	
}



//displaying the array elements
void displayArray(int size,int array[]){
	int i;
	for(i=0;i<size;i++){
		printf("%d\n",array[i]);
	}
}


//Driver program to run the above functions
int main(){
	int num,index;
	//initialising the array
	int array[11] = {3,5,7,9,10,14,16,35,67,89,90};
	printf("Dispalying the array elements");
	//calling the display function
	displayArray(11,array);
	//entering the element to be search
	printf("enter the number to  search:");
	scanf("%d",&num);
	//calling the binarysearch function
	index = binarySearch(0,11-1,array,num);
	if(index==-1){
		printf("entered number doesnot exist\n");
	}
	else{
		printf("position of %d is %d\n:",num,index);
	}
return 0;
}
