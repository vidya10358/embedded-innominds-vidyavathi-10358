
/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
M.VIDYAVATHI          9603477198            mvidyareddy1212@gmail.com           10358

implementation of ipc using pipes
*******************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

char str[20] = "*******";
int main()
{
	int pipearray[2];
	pipe(pipearray);
	//fork process to read data from pipe
	if(fork())
	{
		printf("in parent :str :%s\n",str);
		read(pipearray[0],str,20);
		printf("in parent :%s\n",str);
		exit(0);
	}else{//process to write data to the file
		printf("in child: str%s\n",str);
		strcpy(str,"vidya");
		printf("in child:str %s\n",str);
		sleep(5);
		write(pipearray[1],str,strlen(str)+1);
		exit(0);
		}
	
}

/*************************************OUTPUT OF THE ABOVE PROGRAM************************************
in parent :str :*******
in child: str*******
in parent :vidya
in child:str vidya
*****************************************************************************************************/
