/******************************************************************************************************
NAME                           MOBILENUMBER                     EMAILID                   EMPLOYEEID
-------------------------------------------------------------------------------------------------------
M.VIDYAVATHI                   9603477198                   mvidyareddy1212@gmail.com       10358
-------------------------------------------------------------------------------------------------------
This program is the implementation of BubbleSort
*******************************************************************************************************/
#include<iostream>
#include<stdlib.h>


using namespace std;
//declaring the sorting class
class Sorting
{
	//declaring the variables
	int *array;
	int size;
	public:
	// default constructor for sorting class
	Sorting(int size)
	{
		this->size=size;
		array=new int[size];
		cout<<"enter the array elements of  size:"<<size<<endl;
		for(int i=0;i<size;i++)
		{
			cin>>array[i];
		}
	}
	//destructor for sorting class
	~Sorting()
	{
		delete array;
	}
	//methods decalration
	void bubbleSort();
	void printArray();
};
//method for bubblesort
void Sorting :: bubbleSort()
{
	
	int i,j,temp;
	for(i=0;i<size-1;i++)
	{
		for(j=0;j<size-i-1;j++)
		{
			if(array[j]>array[j+1])
			{
				temp = array[j];
				array[j] = array[j+1];
				array[j+1] = temp;
			}
		}
	}
}
//method for printing the array elements
void Sorting :: printArray()
{
	int i;
	for(i=0;i<size;i++)
		cout<<array[i]<<" ";
	cout<<endl;
}
//Driver program to test above function 
int main()
{
	Sorting *sort = new Sorting(5);
	cout<<"array elements before sorting:"<<endl;
	sort->printArray();
	sort->bubbleSort();
	cout<<"array elements after sorting:"<<endl;
	sort->printArray();
	return 0;		
			
}
/**************************************OUTPUT OF THE ABOVE PROGRAM***********************
imvizag@administrator-OptiPlex-3020:~/Sorting$ ./a.out
enter the array elements of  size:5
3 2 6 5 3 
array elements before sorting:
3 2 6 5 3 
array elements after sorting:
2 3 3 5 6 */
					
