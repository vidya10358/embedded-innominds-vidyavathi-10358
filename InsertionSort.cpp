/******************************************************************************************************
NAME                           MOBILENUMBER                     EMAILID                   EMPLOYEEID
-------------------------------------------------------------------------------------------------------
M.VIDYAVATHI                   9603477198                   mvidyareddy1212@gmail.com       10358
-------------------------------------------------------------------------------------------------------
This program is the implementation of insertion sort
******************************************************************************************************/
#include<iostream>
#include<stdlib.h>


using namespace std;
//declaring the sorting class
class Sorting
{
	//declaring the variables
        int *array;
        int size;
        public:
	// default constructor for sorting class
        Sorting(int size)
        {
                this->size=size;
                array=new int[size];
                cout<<"enter the array elements of size:"<<size<<endl;
                for(int i=0;i<size;i++)
                {
                        cin>>array[i];
                }
        }
	// destructor for sorting class
        ~Sorting()
        {
                delete array;
        }
	//methods declaration
        void InsertionSort();
        void printArray();
};
//method for insertion sort
void Sorting :: InsertionSort()
{

        int i,j,temp;
        for(i=0;i<size;i++)
        {
                j=i-1;
		temp = array[i];
		while(j>=0 && array[j]>temp)
		{
			array[j+1] = array[j];
			j--;
		}
		array[j+1] = temp;
	}
}

//method for printing the array elements                                                                                                    viv
void Sorting :: printArray()
{
        int i;
        for(i=0;i<size;i++)
                cout<<array[i]<<" ";
        cout<<endl;
}
//Driver program to test above functions
int main()
{
        Sorting *sort = new Sorting(5);
	cout<<"array elements before sorting:"<<endl;
	sort->printArray(); 
        sort->InsertionSort();
        cout<<"array elements after sorting:"<<endl;
	sort->printArray();
        return 0;

}
                                                                                                                                     
/****************************************OUTPUT OF THE ABOVE PROGRAM**************************************
imvizag@administrator-OptiPlex-3020:~/Sorting$ ./a.out
enter the array elements of size:5
5 2 9 1 0
array elements before sorting:
5 2 9 1 0 
array elements after sorting:
0 1 2 5 9 */

