/******************************************************************************************************
NAME                           MOBILENUMBER                     EMAILID                   EMPLOYEEID
-------------------------------------------------------------------------------------------------------
M.VIDYAVATHI                   9603477198                   mvidyareddy1212@gmail.com       10358
-------------------------------------------------------------------------------------------------------
This program is the implementation of single linked list where we can add a data at any position and 
delete the node at any position and we can update the data 
**********************************************************************************************************/
#include<iostream>
#include <stdlib.h>
using namespace std;
//creating a structure of type node
struct node {
	//declaring the variables
	int data;
	node *next;//self-referential structure
};
int length = 0;

//creating a class of type SingleLinkedList
class SingleLinkedList 
{
	//declaring the variables
	struct node *head;
	public:
	//default constructor
	SingleLinkedList()
	{
		head = NULL;
	}	
	//member functions
	void addDataAtPosition(int,int);
	void deleteDataAtPosition(int);
	void displayList();
	void updateData(int position);
	void menu();
}; 
//method to add data at position in the linked list
void SingleLinkedList :: addDataAtPosition(int position,int data)  
{
		if(position <0 || position > length) 
		{
			cout<<"we cannot add at the position "<<position <<endl;
			cout<<"The length of the list is only "<<length<<endl;
		} 
		else if(position==0)
		{
			struct node *newnode=new node;
			length++;
			if (head == NULL) 
			{
				head = newnode;
				head->data = data;
				head->next = NULL;
				return;
			}
			newnode->data = data;
			newnode->next = head;
			head = newnode;
		} 
		else if(position==length) 
		{
			struct node  *temp;
			struct node *newnode=new node;
			length++;
			temp=head;
			while (temp->next != NULL)
			{
				temp = temp->next;  
			}
			temp->next = newnode;
			newnode->data = data;
			newnode->next = NULL;

		}
		else 
		{
			int index=1;
			node *newnode=new node;
			newnode->data = data;
			struct node *currentnode=head;
			struct node *previousnode=head;
			while(index !=position) {
				previousnode=currentnode;
				currentnode=currentnode->next;
				index=index+1;
			}
			previousnode->next=newnode;
			newnode->next=currentnode;
			length++;
		}
	}
//method to display the data in the linked list
void SingleLinkedList :: displayList(void) 
{
	if(head == NULL) 
	{
		cout<<"The list is empty "<<endl;
	} 
	else 
	{
		struct node *temp=head;
		while(temp != NULL) 
		{
			cout<<temp->data<<endl;
			temp=temp->next;
		}
	}
		cout<<endl;
		cout<<"The length of the list is"<<" "<<length<<endl;
}

//method to delete data at a particular position from the linked list
void SingleLinkedList :: deleteDataAtPosition(int position) 
	{
		if(position <=0 || position > length) 
		{
			cout<<"we cannot able to delete that position"<<endl<<position;
		}
		else if(position==1) 
		{
			struct node *temp=head;
			head=head->next;
			delete temp;
			temp=NULL;
			length=length-1;
		} 
		else 
		{
			int index=1;
			struct node *currentnode=head;
			struct node *previousnode=head;
			while(index !=position) 
			{
				previousnode=currentnode;
				currentnode=currentnode->next;
				index=index+1;
			}
			previousnode->next=currentnode->next;
			delete currentnode;
			length--;
		}
	}
//method to update the data in linked list
void SingleLinkedList :: updateData(int position) 
{
	if(position < 0 || position > length) 
	{
        	 cout<<"cannot update the data at that position:"<<position<<endl;
                       
        }
       	int index;
        struct node *temp=head;
        for(index=1;index<position;index++) 
	{
		temp=temp->next;
        }
                cout<<"enter the data to be update:"<<endl;
                cin>>temp->data;
}

//method to display menu
void SingleLinkedList ::  menu() 
	{
		int choice;
		int  data;
		int position;

		while(1) 
		{
			cout<<"***********************************************"<<endl;
			cout<<"1.add the Data At Position"<<endl<<"2.delete Data At Position"<<endl<<"3.update the Data"<<endl<<"4.display"<<endl<<"5.exit"<<endl;
			cout<<"enter your choice \n";
			cin>>choice;
			switch(choice) 
			{
					
				case 1:	cout<<"***************************************"<<endl;
					cout<<"Enter position to be added into the list";
					cin>>position;
					cout<<"enter the data to be added"<<endl;
					cin>>data;
					addDataAtPosition(position,data);
					break;
				case 2:	cout<<"*****************************************"<<endl;
					cout<<"enter the position to be deleted";
					int position;
					cin>>position;
					deleteDataAtPosition(position);
					cout<<"displaying the elements in the list after deletion"<<endl;
					displayList();
					break;
				case 3:cout<<"************************************************"<<endl; 
					cout<<"enter the position to be updated "<<endl;
					cin>>position;
					updateData(position);
					cout<<"displaying the list elements after updation "<<endl;
					displayList();
					break;
				case 4:	cout<<"Displaying the data in the list"<<endl;
					displayList();
					break;
				case 5: exit(0);
				default:cout<<"Please enter valid input"<<endl;      
			}

		}
	}

int main() 
{
	SingleLinkedList list;//creating object of type Single linked list
	list.menu();
	return 0;
}
/****************************************OUTPUT OF THE ABOVE PROGRAM**************************************
imvizag@administrator-OptiPlex-3020:~/linkedlist$ ./a.out
***********************************************
1.add the Data At Position
2.delete Data At Position
3.update the Data
4.display
5.exit
enter your choice 
1
***************************************
Enter position to be added into the list0
enter the data to be added
2
***********************************************
1.add the Data At Position
2.delete Data At Position
3.update the Data
4.display
5.exit
enter your choice 
1
***************************************
Enter position to be added into the list1
enter the data to be added
6
***********************************************
1.add the Data At Position
2.delete Data At Position
3.update the Data
4.display
5.exit
enter your choice 
1
***************************************
Enter position to be added into the list2
enter the data to be added
8
***********************************************
1.add the Data At Position
2.delete Data At Position
3.update the Data
4.display
5.exit
enter your choice 
4
Displaying the data in the list
2
6
8

The length of the list is 3
***********************************************
1.add the Data At Position
2.delete Data At Position
3.update the Data
4.display
5.exit
enter your choice 
2
*****************************************
enter the position to be deleted1
displaying the elements in the list after deletion
6
8

The length of the list is 2
***********************************************
1.add the Data At Position
2.delete Data At Position
3.update the Data
4.display
5.exit
enter your choice 
3
************************************************
enter the position to be updated 
0
enter the data to be update:
9
displaying the list elements after updation 
9
8

The length of the list is 2
***********************************************
1.add the Data At Position
2.delete Data At Position
3.update the Data
4.display
5.exit
enter your choice 
5*/













