/**********************************************************************************************************************
   NAME                PHONE NUMBER                      EMAILID                             EMPLOYEE-ID
 ---------------------------------------------------------------------------------------------------------------------
 M.VIDYAVATHI           9603477198                     mvidyareddy1212@gmail.com               10358
 ---------------------------------------------------------------------------------------------------------------------

 This is the implementation of userdefined function for string concatination

 ********************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
//logic for userdefined function for string concatination
char* userStrcat(char *str1,char *str2){
	int i=0,j=0;
	while(str1[i]!='\0'){
		i++;
	}
	while(str1[i]=str2[j]){
		i++;
		j++;
	}
	return str2;

} 
int main(){
	int res;
	char s1[50]="what is ur name",s2[50]="....my name is vidya";
	printf("**********Before concatination**********\n");
	printf("string 1 = %s\n",s1);
	printf("string 2 = %s\n",s2);
	userStrcat(s1,s2);
	printf("***********After Cancatination**********\n");
	printf("string 1=%s\n",s1);
}

/***********************************************OUTPUT OF THE PROGRAM******************************************
**********Before concatination**********
string 1 = what is ur name
string 2 = ....my name is vidya
***********After Cancatination**********
string 1=what is ur name....my name is vidya
**************************************************************************************************************/
