/**********************************************************************************************************************
   NAME                PHONE NUMBER                      EMAILID                             EMPLOYEE-ID
 ---------------------------------------------------------------------------------------------------------------------
 M.VIDYAVATHI           9603477198                     mvidyareddy1212@gmail.com               10358
 ---------------------------------------------------------------------------------------------------------------------
This progarm is the implementation of userdefined strtok function
*********************************************************************************************************************/



#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int main() {
	//initialising the string
	char string[]="Hii how are you";
	//allocating the memory by using dynamic memory allocation
	char * token=(char *)malloc(strlen(string)*sizeof(char));
	char str[2]=" ";
	int i=0,j=0;
	//logic for userdefined strtok function
	for(i=0;i<strlen(string);i=j+1) {
		if(token!=NULL)
			j=i;
		while(string[j]!=str[0])  {
			if(string[j]=='\0') break;
			j++;
		}
		strncpy(token,string+i,j-i);
		token[j-i]='\0';
		printf("%s",token);
		printf("\n");
	}
	return 0;

}
/*********************************************OUTPUT OF THE PROGRAM***************************************
Hii
how
are
you
***********************************************************************************************************/ 
