 /**********************************************************************************************************************
   NAME                PHONE NUMBER                      EMAILID                             EMPLOYEE-ID
 ---------------------------------------------------------------------------------------------------------------------
 M.VIDYAVATHI           9603477198                     mvidyareddy1212@gmail.com               10358
 ---------------------------------------------------------------------------------------------------------------------

 This program is the implementation of reversing the float number

 ************************************************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

int main() {
	//character array for storing the float numbers
	char str[20];
	int count=0,number=0,i;

	printf("enter the float value:");
	scanf("%s",str);

	//counting the digits after '.'
	for(i=strlen(str)-1;i>=0;i--) {
		if(str[i]!='.')
			count++;
		else 
			break;
	}
	//converting the float array into integer number.
	for(i=0;i<strlen(str);i++) {
		if(str[i]>='0' && str[i]<='9')
			number=(number*10)+(str[i]-48);
	}
	for(i=0;i<strlen(str);i++) {
		if(i==count) {
			str[i]='.';
		}
		else {
			str[i]=(number%10)+48;
			number=number/10;
		}
	}
	printf("The reversed number = %s\n",str);
}

/********************************OUTPUT OF THE PROGARM*******************************************************
enter the float value:34526.7865
The reversed number = 5687.62543
*************************************************************************************************************/
