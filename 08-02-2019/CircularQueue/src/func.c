#include"header.h"

//add the student details at the end
struct student *enqueue(struct student *ptr) {
	struct student *newnode=NULL,*temp=NULL;
	newnode=malloc(sizeof(struct student));
	if(newnode==NULL) {
		printf("Error memory allocation is not done\n");
		return NULL;
	}

	if(count==SIZE) {
		printf("Queue is full\n");
		return ptr;
	}

	printf("enter the roll\n");
	scanf("%d",&newnode->roll);
	printf("enter the name\n");
	scanf("%s",newnode->name);
	printf("enter the percentage\n");
	scanf("%f",&newnode->per);


	if(ptr==NULL) {
		count++;
		ptr=newnode;
		ptr->next = newnode;
	}

	else {
		count++;
		temp=ptr;
		while(temp->next!=ptr) {
			temp=temp->next;
		}
		temp->next=newnode;
		newnode->next = ptr;
	}

	return ptr;
}

//deleting the student details at the first
void dequeue(struct student **ptr) {
	struct student *temp=NULL;
	struct student *temp1=NULL;

	if(*ptr==NULL) {
		printf("Queue is empty\n");		
	}

	else if(count==1 ) {
		free(*ptr);
		*ptr=NULL;
		count--;
	}
	else {
		temp=*ptr;
		*ptr=(*ptr)->next;
		for(temp1=*ptr;temp1->next!=temp;temp1=temp1->next);
		temp1->next=*ptr;
		free(temp);
		temp=NULL;
		count--;
	}
}

//displaying the student details

void display(struct student *ptr) {
	struct student *temp=NULL;
	temp=ptr;
	if(temp==NULL) {
		printf("Queue is empty\n");
		return;
	}
	
	printf("-----------------student details are ------------------\n");
	do {
		printf("student id=%d\nname=%s\npercentage=%f\n",temp->roll,temp->name,temp->per);
		temp=temp->next;
	printf("-------------------------------------------------------\n");
	} while(temp!=ptr);
}

//searching the student details based upon roll number

int search(struct student *ptr, int d) {
	if(ptr==NULL) {
		printf("the queue is empty\n");
		return 0;
	}

	while(ptr) {
		if(ptr->roll==d) {
			printf("------------------------------------------\n");
			printf("the found details with that id are\n");
			printf("id=%d\nname=%s\npercentage=%f\n",ptr->roll,ptr->name,ptr->per);
			printf("------------------------------------------\n");
			return 1;
		}
		ptr=ptr->next;
	}
	return 0;

}
