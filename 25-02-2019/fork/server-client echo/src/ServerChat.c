#include"Server.h"
//function for printing the error message
void error(char *msg){
	perror(msg);
	exit(1);
}
//In main method we  create the socket,we bind the ip address and port number to the socket using bind function ,listen waits for the client and accept act as a blocking function when no client is connected
int main(int argc, char **argv){
	int sockfd,newSockfd,portNum;
	char buf[1024];
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	socklen_t cliLen;
	if(argc<2){
		fprintf(stderr,"ERROR, no port available\n");
		exit(1);
	}
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	portNum = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNum);
	if(bind(sockfd,(struct sockaddr*) &serv_addr, sizeof(serv_addr))<0)
		error("ERROR ON BINDING");
	listen(sockfd,5);
	cliLen = sizeof(cli_addr);
	while(1){

		int ret = -1;
		ret = fork();
		if(ret == 0) {//child process
		memset(buf,0,sizeof(buf));
		n = read(newSockfd,buf,1024);
		if(n<0)
			error("ERROR ON READING");
		printf("client= %s\n",buf);
		n = write(newSockfd,buf,strlen(buf));
		if(n<0)
			error("ERROR ON WRITING");
		} else {//parent process

			newSockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cliLen);
			if(newSockfd<0)
				error("ERROR IN ACCEPTING");
		}	


	}
	//closing the socket file descriptors
	close(newSockfd);
	close(sockfd);
	return 0;
}
