#include"Client.h"
//function for printing the error message
void error(char *msg){
	perror(msg);
	exit(1);
}
//In main method we are creating the socket and conneting to the server
int main(int argc, char **argv){
	int sockfd,portNum;
	char buf[1024];
	struct hostent *server;
	struct sockaddr_in serv_addr;
	int n;
	if(argc<3){
		fprintf(stderr,"usage %s hostname port\n",argv[0]);
		exit(0);
	}
	portNum = atoi(argv[2]);
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	server = gethostbyname(argv[1]);
	if(server == NULL){
		fprintf(stderr,"error,no such host\n");
		exit(0);
	}
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	strncpy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port = htons(portNum);
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
		error("ERROR IN CONNECTING");

	 int ret=-1;
	 //creating the fork
         ret=fork();
         if(ret == 0)//child process
         {
                while(1){
                       memset(buf,0,sizeof(buf));
                       fgets(buf,1024,stdin);
                       n = write(sockfd,buf,strlen(buf));
                       if(n<0)
                               error("ERROR ON WRITING");
                        }
       }
         else//parent process
         {
                 while(1)
                 {
                       memset(buf,0,sizeof(buf));
                       n = read(sockfd,buf,1024);
                       if(n<0)
                               error("ERROR ON READING");
                       printf("client= %s\n",buf);
                 }
         }
	//closing the file descriptor
       close(sockfd);
return 0;
}
