/*************************************************************************************************************************
 NAME                           EMAIL-ID                        PHONE NUMBER                    EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------------------------
M. VIDYAVATHI                 mvidyareddy1212@gmail.com         9603477198                         10358
 -------------------------------------------------------------------------------------------------------------------------
 This program is the implementation checking given number is even or odd.
**************************************************************************************************************************/

#include<stdio.h>

int main(){
	//declaring the variables
	int data;
	printf("Enter the data\n");
	scanf("%d",&data);
	//condition for even or odd
	if(data&1){
		printf("odd number\n");
	}else{
		printf("Even number\n");
	}
	return 0;
}
/***************************OUTPUT FOR THE ABOVE PROGRAM**********************************
Enter the data
59
odd number
******************************************************************************************/
