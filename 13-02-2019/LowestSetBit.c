/*************************************************************************************************************************
 NAME                           EMAIL-ID                        PHONE NUMBER                    EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------------------------
 M.VIDYAVATHI           mvidyareddy1212@gmail.com            9603477198                            10358
 -------------------------------------------------------------------------------------------------------------------------
 This program is the implementation of checking the last set bit.
 **************************************************************************************************************************/


#include<stdio.h>
int main(){
	int data;
	printf("enter the data\n");
	scanf("%d",&data);
	for(int i=0;i<=31;i++){
		if((data>>i)&1){
			printf("%d\n",i+1);
			break;
		}

	}
	return 0;
}

/*******************************OUTPUT**************************
 
enter the data
7
1
--------------------------------------------
enter the data
4
3


*************************************************************/
