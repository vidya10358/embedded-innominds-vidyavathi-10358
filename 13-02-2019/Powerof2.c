/*************************************************************************************************************************
 NAME                           EMAIL-ID                        PHONE NUMBER                    EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------------------------
 M.VIDYAVATHI           mvidyareddy1212@gmail.com            9603477198                            10358
 -------------------------------------------------------------------------------------------------------------------------
 This program is the implementation of counting number of digits in the given data.
 **************************************************************************************************************************/

#include<stdio.h>
int main(){
	int data;
	int count = 0;
	printf("enter the data\n");
	scanf("%d",&data);
	for(int i=0;i<=31;i++){
		if((data>>i)&1){
			count++;
		}
	}
	if(count == 1){
		printf("%d is the power of 2\n",data);
	}
	else{
		printf("%d is not the power of 2\n",data);
	}
	return 0;
}
/************************OUTPUT********************************
enter the data
5
5 is not the power of 2
-------------------------------------
enter the data
16
16 is the power of 2
***************************************************************/
