/*************************************************************************************************************************
 NAME                           EMAIL-ID                        PHONE NUMBER                    EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------------------------
 M. VIDYAVATHI           mvidyareddy1212@gmail.com           9603477198                        10358
 -------------------------------------------------------------------------------------------------------------------------
 This program is the implementation of swapping the two elememts using X-OR operation.
**************************************************************************************************************************/

#include<stdio.h>
int main(){
	//declaring the variables
	int data1,data2;
	printf("Enter first element\n");
	scanf("%d",&data1);
	printf("Enter second element\n");
	scanf("%d",&data2);
	printf("***Before swapping***\nfirst element = %d\nsecond element =  %d\n",data1,data2);
	//logic for swapping the elements
	data1^=data2^=data1^=data2;
	printf("\n***After swapping***\nfirst element = %d\nsecond element = %d\n",data1,data2);
}
/*************************************OUTPUT FOR THE ABOVE PROGRAM************************************************************
Enter first element
48
Enter second element
65
***Before swapping***
first element = 48
second element =  65

***After swapping***
first element = 65
second element = 48
busam@busam-Lenovo-G505:~/13-2-2019$ 
*********************************************************************************************************************************/
