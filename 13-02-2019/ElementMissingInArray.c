/*************************************************************************************************************************
 NAME                           EMAIL-ID                        PHONE NUMBER                    EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------------------------
 M.VIDYAVATHI           mvidyareddy1212@gmail.com                9603477198                        10358
 -------------------------------------------------------------------------------------------------------------------------
 This program is the implementation of finding the missing element in the array.
**************************************************************************************************************************/

#include<stdio.h>

int main(){
	int size;
	printf("Enter the array size\n");
	scanf("%d",&size);
	int arr[size-1],total=0,total1=0;
	for(int i=1;i<=size;i++){
		total1 = total1+i;
	}
	for(int i=0;i<size-1;i++){
		printf("Enter the data\n");
		scanf("%d",&arr[i]);
		total = total+arr[i];
	}
	printf("Missing data is %d\n",(total1-total));
	return 0;
}

/****************************************OUTPUT FOR THE ABOVE PROGRAM*******************************************************
Enter the array size
8
Enter the data
2
Enter the data
4
Enter the data
6
Enter the data
5
Enter the data
1
Enter the data
3
Enter the data
7
Missing data is 8 
****************************************************************************************************************************/
