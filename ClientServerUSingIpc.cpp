/**************************************************************************************************
  NAME                  PHONE NUMBER           EMAIL ID
M.VIDYAVATHI      	9603477198             mvidyareddy1212@gmail.com
***************************************************************************************************
This program is implementation of IPC mechanism which include concept of shared memeory,thread
and semaphore.Here we are taking a common shared memory, which is being accessed by client and 
server to get the message.
**************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream> 
#include <semaphore.h>
#include <sys/ipc.h> 
#include <sys/shm.h>
#include <unistd.h>

using namespace std;
//decalring the semaphore variables and semaphore id
static sem_t bin_sem;
static int shmid;
static char *name;
//creating a class of type client
class Client {

	pthread_t thread_id1,thread_id2,thread_id3;

	public :
	//default constructor of type client 
	Client() {

		key_t key = ftok("myfile1",85);
		shmid = shmget(key,1024,0666|IPC_CREAT); 
		char  *str = (char*) shmat(shmid,(void*)0,0); 
		cout<<str<<endl;
		shmdt(str);
	}
	//method for taking the client name
	static void* clientName(void *ptr) {
		sem_wait(&bin_sem);
		cout<<"enter client name :"<<endl;
		name=(char*) shmat(shmid,(void*)0,0); 
		cin>>name;
		sleep(10);
		sem_post(&bin_sem);
	}
	//method for creating the threads
	void clientThread() {

		pthread_create( &thread_id1, NULL,clientName, (void*) NULL);
		pthread_create( &thread_id2, NULL,clientName, (void *) NULL);
		pthread_create( &thread_id3, NULL,clientName, (void *) NULL);
		joinThread();
	}
	//method for joining the threads
	void joinThread() {
		pthread_join( thread_id1, NULL); 
		pthread_join( thread_id2, NULL);
		pthread_join( thread_id3, NULL);
	}
};
int main()
{
	Client c;
	sem_init(&bin_sem,0,1);
	c.clientThread();
	return 0; 
}

/****************Output of the program****************************************
imvizag@administrator-ThinkCentre-M82:~/linux$ ./a.out 
hello

enter client name :
javed
enter client name :
hema
enter client name :
vidya
*****************************************************************************/
