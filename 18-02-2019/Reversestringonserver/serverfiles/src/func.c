
#include"Server.h"


char * reverse(char * array) {
        char temp;
        int l,i,j,a,b;
        l=strlen(array)-1;
	//logic for reversing the string
        for(i=0,j=l;i<j;i++,j--) {
                temp=array[i];
                array[i]=array[j];
                array[j]=temp;
        }
	//logic for reversing the words
        for(i=0;array[i];i++,a=0,b=0) {
                a=i;
                while(array[i]!=32) {
                        if(array[i]==0)
                                break;
                        b=i;
                        i++;
                }
                for(;a<b;a++,b--) {
                        temp=array[a];
                        array[a]=array[b];
                        array[b]=temp;
                }
        }
        array[i]=0;
        return array;

}
