/************************************************************************************************************
    NAME 		MOBILE NUMBER 			EMAIL-ID 			EMPLOYEE-ID
------------------------------------------------------------------------------------------------------------
M. VIDYAVATHI	         9603477198		mvidyareddy1212@gmail.com		  10358
---------------------------------------------------------------------------------------------------------------
This program is the implementation of convolution of two matrixes
*********************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#define SIZE 3
//function for printing the matrix
void printingMatrixElements(int rows,int columns,int arr[rows][columns]){
	int i,j;
	
	for(i=0;i<rows;i++){
		for(j=0;j<columns;j++){
			printf("%d\t",arr[i][j]);
		}
		printf("\n");
	}
}

int main(){
	int row,column,i,j,sum=0,k,l;
	srand(getpid());
	printf("Enter row and column values\n");
	scanf("%d %d",&row,&column);
	//declaring the main matrix
	int arr[row][column];
	//declaring the resultant matrix 
	int resultant[row][column];
	//declaring the window matrix
	int window[SIZE][SIZE];
	for(i=0;i<row;i++){
		for(j=0;j<column;j++){
			//generating the main matrix elements randomly
			arr[i][j]=rand()%10;
			resultant[i][j] = 0;
		}
	}
	printf("Displaying the main matrix\n");
	printingMatrixElements(row,column,arr);
	printf("Displaying the resultant matrix before convolution\n");
	printingMatrixElements(row,column,resultant);
	for(i=0;i<SIZE;i++){
		for(j=0;j<SIZE;j++){
			//generating the window matrix elements randomly
			window[i][j]=rand()%10;
		}
	}
	
	printf("Displaying the window matrix\n");
	printingMatrixElements(SIZE,SIZE,window);
	//logic for convolution
	for(i=0;i<row-2;i++){
		for(j=0;j<column-2;j++){
			sum = 0;
			for(k=0;k<=2;k++){
				for(l=0;l<=2;l++){
					sum+=arr[i+k][j+l]*window[k][l];
				}
			}
			resultant[i+1][j+1]=sum;
		}
		
	}
	printf("Displaying the resultant matrix after convolution\n");
	printingMatrixElements(row,column,resultant);
	return 0;
}
/************************************OUTPUT OF THE ABOVE PROGRAM**************************************
Enter row and column values
7 7
Displaying the main matrix
3	6	0	5	6	0	4	
1	0	5	7	7	6	7	
6	1	0	1	3	2	4	
4	8	1	6	5	1	2	
9	9	9	2	7	2	7	
5	2	4	7	2	9	4	
0	7	3	8	8	5	9	
Displaying the resultant matrix before convolution
0	0	0	0	0	0	0	
0	0	0	0	0	0	0	
0	0	0	0	0	0	0	
0	0	0	0	0	0	0	
0	0	0	0	0	0	0	
0	0	0	0	0	0	0	
0	0	0	0	0	0	0	
Displaying the window matrix
4	7	4	
8	6	7	
7	3	8	
Displaying the resultant matrix after convolution
0	0	0	0	0	0	0	
0	142	138	217	234	240	0	
0	138	185	189	206	216	0	
0	280	226	223	169	213	0	
0	338	285	264	262	225	0	
0	260	318	275	316	311	0	
0	0	0	0	0	0	0
*********************************************************************************************************/ 
