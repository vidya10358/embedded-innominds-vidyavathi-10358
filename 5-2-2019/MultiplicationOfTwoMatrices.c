/************************************************************************************************************
    NAME 		MOBILE NUMBER 			EMAIL-ID 			EMPLOYEE-ID
------------------------------------------------------------------------------------------------------------
M. VIDYAVATHI	         9603477198		mvidyareddy1212@gmail.com		  10358
------------------------------------------------------------------------------------------------------------

In this program we perform the multiplication of two matrices. Performing multipication only if columns of first matrix equal to 
rows of second matrix.

**********************************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

//function to print the elements of the matrix
void printingMatrixElements(int rows,int columns,int arr[rows][columns]){
        int i,j;
        for(i=0;i<rows;i++){
                for(j=0;j<columns;j++){
                        printf("%d\t",arr[i][j]);
                }
                printf("\n");
        }
}

int main(){
	//declaring the variables
        int row,column,i,j,sum,k,row1,column1;
        srand(getpid());
	//reading the row and column values of the first matrix
        printf("Enter row and column values of first matrix \n");
        scanf("%d %d",&row,&column);
	//reading the row and column values of the second matrix
        printf("Enter row and column value of second matrix\n");
        scanf("%d %d",&row1,&column1);
	if(column!=row1){//condition to check wheather matrix multiplication is possible or not
		printf("it is not possible to multiply");
		}else{
			srand(getpid());
			//declaring the arrays
			int matrix[row][column];
			int matrix1[row1][column1];
			int resultant[row][column1];
			//assigning the values for first matrix
			for(i=0;i<row;i++){
				for(j=0;j<column;j++){
					matrix[i][j]=rand()%10;
				}
			}
			printf("printing the first matrix\n");
			printingMatrixElements(row,column,matrix);
			//assigning the values for the second matrix
			for(i=0;i<row1;i++){
				for(j=0;j<column1;j++){
					matrix1[i][j]=rand()%10;
				}
			}
			printf("printing the second matrix\n");
			printingMatrixElements(row1,column1,matrix1);
			
			for(i=0;i<row;i++){
				for(j=0;j<column1;j++){
					resultant[i][j]=0;
				}
			}
			//printing the matrix elements before multiplication
			printf("printing the resultant matrix before multiplication\n");
			printingMatrixElements(row,column1,resultant);
			//logic for matrix multiplication
			for(i=0;i<row;i++){
				for(j=0;j<column1;j++){
					sum=0;
					for(k=0;k<column;k++){
						sum+=matrix[i][k]*matrix1[k][j];
					}
					resultant[i][j]=sum;
				}
			}

			printf("printing the resultant matrix after multiplication\n");
			printingMatrixElements(row,column1,resultant);

		}
       return 0;	
}

/*******************************************OUTPUT FOR THE ABOVE PROGRAM***************************************************************
Enter row and column values of first matrix 
5
6
Enter row and column value of second matrix
6  
4
printing the first matrix
2	5	7	6	1	3	
5	9	5	2	5	9	
9	8	0	2	1	0	
7	9	0	8	6	9	
8	6	7	4	6	0	
printing the second matrix
3	8	8	2	
4	9	5	9	
8	0	4	5	
2	5	6	4	
7	9	5	7	
1	7	5	7	
printing the resultant matrix before multiplication
0	0	0	0	
0	0	0	0	
0	0	0	0	
0	0	0	0	
0	0	0	0	
printing the resultant matrix after multiplication
104	121	125	136	
139	239	187	222	
70	163	129	105	
124	294	224	232	
154	192	176	163
--------------------------------------------------------------------------------------
Enter row and column values of first matrix 
5
4 
Enter row and column value of second matrix
5
4
it is not possible to multiply
 
****************************************************************************************************************************************/
