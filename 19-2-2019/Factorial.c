/*********************************************************************************************************
NAME                      EMAILID                            PHONE NUMBER                  EMPID
------------------------------------------------------------------------------------------------------------
M.VIDYAVATHI        mvidyareddy1212@gmail.com                 9603477198                   10358
------------------------------------------------------------------------------------------------------------
This program is to find factorial of a given number using recursion
************************************************************************************************************/
#include<stdio.h>
//this function returns the factorial of given number
 int fact(int num) {

	 if(num==0 || num==1) 
		 return 1;
	 else
		return (num*fact(num-1));
 }
int main() {
	int num;
	printf("enter the number to find the factorial :\n");
	scanf("%d",&num);
	while(num<0) {
		printf("factorial cannot be negative......re enter \n");
		scanf("%d",&num);
	}
	int a=fact(num);
	printf("the factorial for the given number %d is: %d\n",num,a);
	return 0;
}
/******************************************OUTPUT OF THE ABOVE PROGRAM*********************************************
enter the number to find the factorial :
6
the factorial for the given number 6 is: 720
********************************************************************************************************************/
