/******************************************************************************************************
NAME                           MOBILENUMBER                     EMAILID                   EMPLOYEEID
-------------------------------------------------------------------------------------------------------
M.VIDYAVATHI                   9603477198                   mvidyareddy1212@gmail.com       10358
-------------------------------------------------------------------------------------------------------
This program is the implementation of queue using linked list where user add the data at the front and 
delete the data from the end
*******************************************************************************************************/
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
using namespace std;
//creating  the structure node
struct node {
		//declaring the variables
		int data;		
		struct node *next;
	}*front,*rare;
//creating the queue class
class Queue {
	//declaring the front and rare pointers 
	struct node *front,*rare;
   public:
	//default constructor for queue
	Queue() {
		front=NULL;
		rare=NULL;
	}
	//declaring the memberfunctions
	int enqueue(int data);
	int dequeue();
	void displayDataInTheQueue();
}; 
//method for enqueue	
int Queue :: enqueue(int data) 
{
	struct node *newnode;
	newnode = new node;
	newnode->data=data;
	if(front==NULL) 
	{
		front=newnode;
		rare=newnode;
		newnode->next=NULL;
		return 0;
	}
	rare->next=newnode;
	rare=newnode;
	return 0;
} 
//method for dequeue
int Queue :: dequeue()
 {
	int data;
	if(front==NULL) 
	{
		cout<<"Queue is empty"<<endl;
		return 0;
	}
	struct node *temp=front;
	data=temp->data;
	front=front->next;
	delete temp;
	temp=NULL;
	return data;
}
//method for displaying the data in the queue
void Queue :: displayDataInTheQueue() 
{
	struct node *temp=front;
	cout<<"\t*****records in queue:*****"<<endl;
	for(;temp!=NULL;temp=temp->next) 
	{
		cout<<temp->data<<endl;
	}
}

//main method
int main(void) {
	//creating the object for queue class 
	Queue queue;
	int choice;
	int data;
	while(1) {
		cout<<"*****MENU*****"<<endl;
		cout<<"1.enqueue\n2.dequeue\n3.display data in queue\n4.exit"<<endl;
		cout<<"enter your choice:"<<endl;
		cin>>choice;
		switch(choice)
 		{
			case 1:	cout<<"enter data:"<<endl;
				cin>>data;
				queue.enqueue(data);
				break;
			case 2: data=queue.dequeue();
                                if(data)
                                cout<<"dequeue value"<<" "<<data<<endl;	
				break;
			case 3:	queue.displayDataInTheQueue();
				break;
			case 4:exit(0);
			default:cout<<"invalid choice"<<endl;
		}
	} 


}
/***********************************************OUTPUT OF THE ABOVE PROGRAM******************************
imvizag@administrator-OptiPlex-3020:~/linkedlist$ ./a.out
*****MENU*****
1.enqueue
2.dequeue
3.display data in queue
4.exit
enter your choice:
1
enter data:
2
*****MENU*****
1.enqueue
2.dequeue
3.display data in queue
4.exit
enter your choice:
1
enter data:
3
*****MENU*****
1.enqueue
2.dequeue
3.display data in queue
4.exit
enter your choice:
1
enter data:
4
*****MENU*****
1.enqueue
2.dequeue
3.display data in queue
4.exit
enter your choice:
3
	*****records in queue:*****
2
3
4
*****MENU*****
1.enqueue
2.dequeue
3.display data in queue
4.exit
enter your choice:
2
dequeue value 2
*****MENU*****
1.enqueue
2.dequeue
3.display data in queue
4.exit
enter your choice:
3
	*****records in queue:*****
3
4
*****MENU*****
1.enqueue
2.dequeue
3.display data in queue
4.exit
enter your choice:
4*/

